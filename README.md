# Assignment/Code Test

This repo is an assignment/test given as a part of the Interview process. The main
idea of this assignment is to create a simple ReactJS application which will
create a `justify text` effect on a provided text.

### Tech Used

* nodejs
* expressjs
* react
* redux
* bootstrap
* webpack


### Installation
First go to the project directory and install npm dependencies.

```sh
$ cd react_test && npm i
```

Then Run the server run following command.
```sh
$ npm start
```

you can now access the application at `http://localhost:3333`.
