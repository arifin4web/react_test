'use strict';

import express from 'express';
import path from 'path';
import favicon from 'serve-favicon';
import logger from 'morgan';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import axios from 'axios';

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/**
 * An additional Route to get lorenipsum text.
 * mainly to avoid CORS Issues.
 */
app.get('/new_loremipsum', function(req, res) {
  console.log("GEtting New L")
  axios.get('http://loripsum.net/api/1/plaintext/long').then((response) => {
      return res.status(200).json({
        success: true,
        message: 'Successfully fetch new lorem ipsum.',
        loremipsum: response.data
      });
    })
    .catch((err) => {
      console.log("AXIOS ERROR >>", err);
      res.status(500).json({
        success: false,
        message: 'Error in fetch new lorem ipsum.',
        error: err
      });
    });
});

// route handlers
app.get('*', function(req, res) {
  res.render('index', {
    title: 'Assignment - Justify Text'
  });
});

export default app;
