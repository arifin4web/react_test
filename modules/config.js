import 'bootstrap';

import toastr from 'toastr';
toastr.options.positionClass = 'toast-bottom-right';

import axios from 'axios';
axios.defaults.baseURL = 'http://localhost:3333/';
