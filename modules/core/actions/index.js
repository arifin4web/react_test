import axios from 'axios';
import {texJustify} from '../libs/justifyText.js'

/**
 * Will dispatch when Some Core Data is needed.
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
export function getCoreData(data) {
    return function (dispatch) {
        dispatch({type: 'GET_CORE_DATA_RESOLVED', payload: { data }});
    }
}

/**
 * This function will be used to update the status `processing` in core reducer
 *
 * @param  {[type]} status [description]
 * @return {[type]}        [description]
 */
export function updateProcessStat(status) {
    return function (dispatch) {
        dispatch({type: 'UPDATE_PROCESSING_RESOLVED', payload: { data: status }});
    }
}

/**
 * will communicate with server method and fetch new text from
 * lorenimsum.net
 *
 * @param  {Function} cb [description]
 * @return {[type]}      [description]
 */
export function fetchText(cb) {
    return function (dispatch) {
        dispatch({type: 'GET_TEXT_PENDING'})
        axios.get('new_loremipsum')//url Here
            .then((response) => {
                dispatch({type: 'GET_TEXT_RESOLVED', payload: response});
                if (cb && typeof cb === 'function') {
                    cb(null, response);
                }
            })
            .catch((err) => {
                dispatch({type: 'GET_TEXT_FAILED', payload: err});
                if (cb && typeof cb === 'function') {
                    cb(err, null);
                }
            });
    }
}

/**
 * Will dispatch delete text.
 *
 * @param  {Function} cb [description]
 * @return {[type]}      [description]
 */
export function delFetchedText(cb) {
    return function (dispatch) {
        dispatch({type: 'DEL_TEXT_RESOLVED'})
    }
}

/**
 * Using Our textJustify Library class this method will
 * dispatch a justified version of provided text within the
 * lineWidth.
 *
 * @param  {[type]}   lineWidth [description]
 * @param  {[type]}   text      [description]
 * @param  {Function} cb        [description]
 * @return {[type]}             [description]
 */
export function justifyText(lineWidth, text, cb) {
    return function (dispatch) {
        console.log("JUSTIFYING TEXT");
        try{
          var justify = new texJustify(lineWidth, text);
          var justifiedText = justify.getJustifiedText();

          dispatch({type: 'JUSTIFY_TEXT_RESOLVED', payload: { data: justifiedText }});
          dispatch({type: 'PUSH_TEXT_STORE_RESOLVED', payload: { justifiedText }});
          if(cb && typeof cb === 'function')
            cb(null, justifiedText);

        }catch(err){
          dispatch({type: 'JUSTIFY_TEXT_FAILED', payload: { err }});
          if(cb && typeof cb === 'function')
            cb(err, null);
        }
    }
}

/**
 * Restore a specic item from the fetched and justified texts from the storage.
 *
 * @param  {[type]}   index [description]
 * @param  {Function} cb    [description]
 * @return {[type]}         [description]
 */
export function restoreText(index, cb) {
    return function (dispatch) {
        dispatch({type: 'RESTORE_TEXT_RESOLVED', payload: { ind: index }});
        if(cb && typeof cb === 'function')
          cb();
    }
}

/**
 * Delete a specific item from the fetched texts.
 * 
 * @param  {[type]}   index [description]
 * @param  {Function} cb    [description]
 * @return {[type]}         [description]
 */
export function delStoredText(index, cb) {
    return function (dispatch) {
        dispatch({type: 'DEL_STORED_TEXT_RESOLVED', payload: { ind: index }});
        if(cb && typeof cb === 'function')
          cb();
    }
}
