'use strict';

import React, {Component} from 'react';
import axios from 'axios';
import toastr from 'toastr';


class FetchTextComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            lineWidth: 50 //Default Line width is set to 50.
        }

        this.makeTextJustify = this.makeTextJustify.bind(this);
        this.deleteStoreText = this.deleteStoreText.bind(this);
        this.updateLineWidth = this.updateLineWidth.bind(this);
        this.restore = this.restore.bind(this);
    }

    /**
     * will keep state value updated with change in the input
     */
    updateLineWidth(event) {
        this.setState({lineWidth: event.target.value});
    }

    /**
     * Main function to fetch and justify texts.
     */
    makeTextJustify() {
        var _this = this;
        /**
         * [longestWordLen description]
         * @param  {[type]} str [description]
         * @return {[type]}     [description]
         */
        var longestWordLen = function(str) {
            var strSplit = str.split(' ');
            var longestWord = strSplit.reduce(function(longest, currentWord) {
                if (currentWord.length > longest.length)
                    return currentWord;
                else
                    return longest;
                }
            , "");

            return longestWord.length;
        }

        //First we need to fetch new sting
        this.props.updateProcessStat(true);
        console.log('Request to fetch new text');
        this.props.fetchText(function(err, resp) {
            if (resp) {
                toastr.success('New text fetched Successfully.', 'Fetching Text!');
                // Now that text is here, lets validate linewidth.
                var lineWidth = _this.state.lineWidth;
                if(lineWidth >= longestWordLen(_this.props.core.fetched_text)){
                  console.log("Valid Line Width");
                  //Now lets justify
                  _this.props.justifyText(lineWidth, _this.props.core.fetched_text, function(err, resp) {
                      if(resp){
                        _this.props.updateProcessStat(false);
                        toastr.success('Successfully justify a text.', 'Justify Text!');
                      }else if(err){
                        _this.props.updateProcessStat(false);
                        toastr.error('Something went wrong while justifing a text.', 'Justify Text!');
                      }
                  });

                }else{
                  _this.props.updateProcessStat(false);
                  toastr.error('Line width cann\'t be less then longest word.', 'Invalid Line Width');
                }

            } else if(err){
                _this.props.updateProcessStat(false);
                toastr.error('Couldn\'t fetch new text.', 'Fetching Text!');
            }
        });
    }

    /**
     * Restore aspecific item from the storage.
     */
    restore(index){
      if(typeof index=='undefined')
        return false;

      var _this = this;
      console.log("Restore",index);
      this.props.restoreText(index, function(){
        toastr.success('Successfully restore item '+(index+1), 'Restoring Text!');
      });
    }

    /**
     * Delete the current text from the store
     */
    deleteStoreText(){

      var current = this.props.texts.current;

      var _this = this;
      console.log("Delete",current);
      this.props.delStoredText(current, function(){
        toastr.success('Successfully delete item '+(current+1), 'Removing Text');
      });
    }

    /**
     * Funciton to render loading bar.
     */
    loadingBar() {
        return (
            <div className="load-bar">
                <div className="bar"></div>
                <div className="bar"></div>
                <div className="bar"></div>
            </div>
        );
    }

    render() {
        return (
            <div className="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">

                <div className="row">
                    <div className="col-xs-4">
                        <div className="form-group label-static">
                            <label htmlFor="lineLength" className="control-label">Max Line Length</label>
                            <input type="number" min='0' className="form-control" value={this.state.lineWidth} onChange={this.updateLineWidth} disabled={this.props.core.processing}/>
                        </div>
                    </div>
                    <div className="col-xs-4">
                        <a className="btn btn-raised btn-primary pull-right"
                           onClick={this.makeTextJustify}
                           disabled={this.props.core.processing || this.state.lineWidth<=0}>Justify Text</a>
                    </div>
                    <div className="col-xs-4">
                        {this.restoreView()}
                        <a className="btn btn-raised btn-danger pull-right"
                          disabled={this.props.core.processing|| !this.props.texts.textLog.length}
                          onClick={this.deleteStoreText}>
                          delete
                        </a>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12 text-right m-b">
                        Showing <strong>{(this.props.texts.textLog.length)? this.props.texts.current +1:this.props.texts.current}</strong> out of <strong>{this.props.texts.textLog.length}</strong> items.
                    </div>
                </div>
                <div className="row">
                    {(this.props.core.processing) ? this.loadingBar(): ''}
                </div>
                <div className="row">
                    <div className="col-xs-12 textBox">
                        <pre className="output">
                          {(this.props.texts.textLog[this.props.texts.current])?this.props.texts.textLog[this.props.texts.current].justifiedText:''}
                        </pre>
                    </div>
                </div>
            </div>
        );
    }

    /**
     * Render the list of the items to be rendered.
     * Used in restore dropdown button.
     */
    restoreListView() {
        return this.props.texts.textLog.map((text, i) => {
            return (
                <li key={i + 1}>
                    <a className="pointer" onClick={this.restore.bind(this, i)}>{i + 1}</a>
                </li>
            );
        })
    }

    /**
     * Render restore render dropdown button.
     */
    restoreView() {
        return (
            <div className="btn-group">
                <button
                  type="button"
                  disabled={this.props.core.processing || !this.props.texts.textLog.length}
                  className="btn btn-primary btn-raised dropdown-toggle"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false">
                    Restore <span className="caret"></span>
                </button>
                <ul className="dropdown-menu">
                    {this.restoreListView()}
                </ul>
            </div>
        );
    }

}

export default FetchTextComponent;
