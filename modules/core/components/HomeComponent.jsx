'use strict';

import React, { Component } from 'react';
import FetchTextComponent from './FetchTextComponent.jsx'

class HomeComponent extends Component {
  render() {
    return (
      <div id='container'>
        <div className="col-xs-12">
          <div className="row">
            <FetchTextComponent
              {...this.props}/>
          </div>
        </div>
      </div>
    );
  }
}

export default HomeComponent;
