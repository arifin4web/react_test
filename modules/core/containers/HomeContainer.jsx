'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {browserHistory} from 'react-router';
import toastr from 'toastr';

import HomeComponent from '../components/HomeComponent.jsx';

import { getCoreData, fetchText, updateProcessStat, justifyText, delFetchedText, restoreText, delStoredText } from '../actions'

class HomeContainer extends Component {
  render() {
    return (
      <HomeComponent
        {...this.props}/>
    );
  }
}

function mapStateToProps(store){
  return {
    core : store.core,
    texts : store.texts
  }
}

function matchDispatchToProps(dispatch){
  return bindActionCreators({
    getCoreData : getCoreData,
    fetchText: fetchText,
    updateProcessStat: updateProcessStat,
    justifyText: justifyText,
    delFetchedText: delFetchedText,
    restoreText: restoreText,
    delStoredText: delStoredText
  }, dispatch)
}

export default connect(mapStateToProps,matchDispatchToProps)(HomeContainer);
// export default HomeContainer;
