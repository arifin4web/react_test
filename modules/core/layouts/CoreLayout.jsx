'use strict';

import React, { Component } from 'react';

class CoreLayout extends Component {
  render() {
    return (
      <div className='container-fluid'>
        {this.props.children}
      </div>
    );
  }
}

export default CoreLayout;
