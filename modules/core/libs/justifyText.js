'use strict';

/**
 * A simple Class to generate a Justified version of
 * the provided text within the LinwWidh limit.
 */
export class texJustify {
  constructor(lineWidth, textString) {
    this._lineWidth = lineWidth;
    this._text = textString;
  }

  /**
   * Function to trim the provided string and
   * split it into words.
   */
  splitIntoWords() {
    var string = this._text;
    string = string.trim();
    //remove trailing newline if any
    string = string.replace(/\r?\n|\r/g, '');
    var words = string.split(" ");
    return words;
  }

  /**
   * Function to take the array and produce the final justified text.
   */
  getJustifiedText() {
    var justifiedArray = this.getJustifiedArray(),
      justifiedText = '';

    for (var i = 0; i < justifiedArray.length; i++) {
      justifiedText += justifiedArray[i];
      if (i < justifiedArray.length - 1)
        justifiedText += '\n';

    }
    return justifiedText;
  }

  /**
   * This function will organize the words in justified lines
   * return an array.
   */
  getJustifiedArray() {
    var trimmed_str = this._text.trim(),
      words = this.splitIntoWords(),
      lineWidth = this._lineWidth,
      result = [],
      totalLen = words.length,
      lastIndex = -1,
      curLen = 0,
      countWord,
      spaceCount,
      extraSpace,
      totalSpace,
      word = '';

    var addSpace = function(word, n) {
      while (n > 0) {
        word += ' ';
        n--;
      }

      return word;
    };

    for (var i = 0; i < totalLen; i++) {
      curLen += words[i].length + 1;

      if (curLen - 1 > lineWidth || i === totalLen - 1) {
        if (curLen - 1 > lineWidth && (i - lastIndex > 1)) {
          curLen -= words[i].length + 1;
          i--;
        }

        countWord = i - lastIndex;
        curLen -= countWord;

        if (countWord === 1) {
          word += words[i];
          word = addSpace(word, lineWidth - curLen);
        } else if (i === totalLen - 1) {
          totalSpace = lineWidth - curLen;

          for (var j = lastIndex + 1; j <= i; j++) {
            word += words[j];

            if (totalSpace > 0) {
              totalSpace--;
              word += ' ';
            }
          }

          if (totalSpace > 0) {
            word = addSpace(word, totalSpace);
          }
        } else {
          // we need one less space then the words
          spaceCount = parseInt((lineWidth - curLen) / (countWord - 1));
          extraSpace = (lineWidth - curLen) % (countWord - 1);

          for (var j = lastIndex + 1; j <= i; j++) {
            word += words[j];

            if (j !== i) {
              word = addSpace(word, spaceCount);

              if (extraSpace > 0) {
                word += ' ';
                extraSpace--;
              }
            }
          }
        }

        result.push(word.trim());
        word = '';
        lastIndex = i;
        curLen = 0;
      }
    }

    return result;
  }
}
