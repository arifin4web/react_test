'use strict';

let defaultData = {
  title: "Justify Text",
  tagLine: "A simple assignment to Justify text only using JavaScript.",
  processing: false,
  fetched_text: '',
  justified_text: ''
};

export const coreDataReducer = (state = defaultData, action) => {

  switch (action.type) {
    case 'GET_CORE_DATA_RESOLVED':
      return Object.assign({}, state, {
        core: action.payload.data
      });
    case 'GET_TEXT_RESOLVED':
      return Object.assign({}, state, {
        fetched_text: action.payload.data.loremipsum
      });
    case 'DEL_TEXT_RESOLVED':
      return Object.assign({}, state, {
        fetched_text: ''
      });
    case 'JUSTIFY_TEXT_RESOLVED':
      console.log("coreDataReducer >>", action);
      return Object.assign({}, state, {
        justified_text: action.payload.data
      });
    case 'UPDATE_PROCESSING_RESOLVED':
      return Object.assign({}, state, {
        processing: action.payload.data
      });
    default:
      return state;
  }
}


let defaultText = {
  textLog: [],
  current: 0
}

export const textDataReducer = (state = defaultText, action) => {

  switch (action.type) {
    case 'PUSH_TEXT_STORE_RESOLVED':
      console.log("coreDataReducer_PUSH >>", action);
      let log = {};
      if (state.textLog.length >= 5) {
        console.log("Exceeding");
        return Object.assign({}, state, {
          textLog: [action.payload, ...state.textLog.slice(0, -1)]
        });
      } else {
        return Object.assign({}, state, {
          textLog: [action.payload, ...state.textLog]
        });
      }
    case 'RESTORE_TEXT_RESOLVED':
      return Object.assign({}, state, {
        current: action.payload.ind
      });
    case 'DEL_STORED_TEXT_RESOLVED':
      console.log("TEXT REDUCER>>", action);
      return Object.assign({}, state, {
        textLog: state.textLog.slice(0, action.payload.ind).concat(state.textLog.slice(action.payload.ind + 1)),
        current: 0
      });
    default:
      return state;
  }
}
