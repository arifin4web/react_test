'use strict';

import { combineReducers } from 'redux';
import { coreDataReducer, textDataReducer } from './core/reducers/index.js';

const reducers = combineReducers({
	core:coreDataReducer,
	texts:textDataReducer
});

export default reducers;
