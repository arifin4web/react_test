'use strict';

import React, { Component } from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import CoreLayout from './core/layouts/CoreLayout.jsx';
import HomeContainer from './core/containers/HomeContainer.jsx';
import NotFoundComponent from './core/components/NotFoundComponent.jsx';

class Routes extends Component {
  render() {
    return (
      <Router history={browserHistory}>
        <Route path='/' component={CoreLayout}>
          <IndexRoute component={HomeContainer} />
        </Route>
        <Route path='*' component={CoreLayout}>
          <IndexRoute component={NotFoundComponent} />
        </Route>
      </Router>
    );
  }
}

export default Routes;
