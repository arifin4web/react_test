'use strict';

import axios from 'axios';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { createStore, applyMiddleware } from 'redux';
import {persistStore, autoRehydrate} from 'redux-persist'

import reducers from './reducers.js';

const store = createStore(
  reducers,
  applyMiddleware(thunk, logger()),
  autoRehydrate()
);

persistStore(store);

export default store;
